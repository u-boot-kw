/*
 * (C) Copyright 2000-2004
 * DENX Software Engineering
 * Wolfgang Denk, wd@denx.de
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef _MKIIMAGE_H_
#define _MKIIMAGE_H_

#include "os_support.h"
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <sha1.h>
#include "fdt_host.h"

#define MKIMAGE_DEBUG

#ifdef MKIMAGE_DEBUG
#define debug(fmt,args...)	printf (fmt ,##args)
#else
#define debug(fmt,args...)
#endif /* MKIMAGE_DEBUG */

#define MKIMAGE_TMPFILE_SUFFIX		".tmp"
#define MKIMAGE_MAX_TMPFILE_LEN		256
#define MKIMAGE_DEFAULT_DTC_OPTIONS	"-I dts -O dtb -p 500"
#define MKIMAGE_MAX_DTC_CMDLINE_LEN	512
#define MKIMAGE_DTC			"dtc"   /* assume dtc is in $PATH */

struct mkimage_params {
	int dflag;
	int eflag;
	int fflag;
	int lflag;
	int vflag;
	int xflag;
	int opt_os;
	int opt_arch;
	int opt_type;
	int opt_comp;
	char *opt_dtc;
	uint32_t addr;
	uint32_t ep;
	char *imagename;
	char *datafile;
	char *imagefile;
	char *cmdname;
};

/*
 * These are image type specific variables
 * and callback functions
 */
struct image_type_params {
	uint32_t header_size;
	void *hdr;
	int (*check_params) (struct mkimage_params *);
	int (*verify_header) (char *, int, struct mkimage_params *);
	void (*print_header) (char *);
	void (*set_header) (char *, struct stat *, int,
					struct mkimage_params *);
};

/*
 * Image type specific function pointers list
 */
struct image_functions {
	void *	(*get_tparams) (void);
};

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#endif /* _MKIIMAGE_H_ */
